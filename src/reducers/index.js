import { combineReducers } from 'redux';
import chat from '../containers/chat/reducer';
import editMessagePage from '../containers/editMessagePage/reducer';

const rootReducer = combineReducers({
  chat,
  editMessagePage
});

export default rootReducer;