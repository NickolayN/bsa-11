import { callApi } from '../helpers/apiHelper';

export const getMessages = async () => {
  const endpoint = '1hiqin.json';

  try {
    const apiResult = await callApi(endpoint, 'GET');

    return apiResult;
  } catch (error) {
    throw error;
  }
}