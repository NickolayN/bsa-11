import React from "react";
import { connect } from 'react-redux';
import * as actions from './actions';
import Textarea from '../../components/textarea/textarea';
import { updateMessage } from "../chat/actions.js";

import './index.css';

class EditMessagePage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      message: ''
    };

    this.onCancel = this.onCancel.bind(this);
    this.onSave = this.onSave.bind(this);
  }

  static getDerivedStateFromProps(props, state) {
    if (props.messageId !== state.messageId) {
      const message = props.messages.find(message => props.messageId === message.id);
      if (message) {
        return {
          message: message.message,
          messageId: props.messageId
        };
      }
    }
    return null;
  }

  onChange(e) {
    const message = e.target.value;
    this.setState(
      {
        ...this.state,
        message
      }
    );
  }

  onCancel() {
    this.props.hidePage();
    this.setState({message: '', messageId: ''});
  }

  onSave() {
    this.props.updateMessage(this.props.messageId, this.state);
    this.props.hidePage();
    this.setState({message: '', messageId: ''});
  }

  getEditMessagePageContent() {
    return (
      <div className="modal">
        <div className="modal-content">
          <h2>Edit message</h2>
          <Textarea
            value={this.state.message}
            onChange={(e) => this.onChange(e)}
          />
          <div className="modal-footer">
            <button onClick={this.onCancel}>Cancel</button>
            <button onClick={this.onSave}>Save</button>
          </div>
        </div>
      </div>
    )
  }

  render() {
    const isShown = this.props.isShown;
    return isShown ? this.getEditMessagePageContent() : null;
  }
}

const mapStateToProps = (state) => {
  return {
    isShown: state.editMessagePage.isShown,
    messageId: state.editMessagePage.messageId,
    messages: state.chat.messages
  }
};

const mapDispatchToProps = {
  ...actions,
  updateMessage
};

export default connect(mapStateToProps, mapDispatchToProps)(EditMessagePage);