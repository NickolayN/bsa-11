import { SHOW_PAGE, HIDE_PAGE } from "./actionTypes";

const initialState = {
  messageId: '',
  isShown: false
};

export default function (state = initialState, action) {
  switch (action.type) {
    case SHOW_PAGE:
      const id = action.payload.id;
      return {
        ...state,
        isShown: true,
        messageId: id
      };

    case HIDE_PAGE:
      return initialState;
  
    default:
      return state;
  }
}