import React from 'react';

function EditButton(props) {
  return (
    <button className="edit-button"
      onClick={props.onClick}
    >
      &#9776;
    </button>
  );
}

export default EditButton;